package jarInvocation;

import java.io.*;
import java.util.concurrent.*;



/**
 * Processの標準・エラー出力内容を取得する。
 * 注意：Process.waitFor()でプロセスが終了しても、入力ストリームが閉じられていない
 * ことがある。出力を取得するにはストリームを強制的にクローズすること。
 */
public abstract class ProcessOutput {
  

  
  private Process process;
  private InputOutput stderr;
  private InputOutput stdout;
  
  public ProcessOutput(final Process process) {
    this.process = process;
    
    stderr = new InputOutput(process.getErrorStream()) {
      protected void output(String line) {
        stderr(line);
      }
    };
    stdout = new InputOutput(process.getInputStream()) {
      protected void output(String line) {        
        stdout(line);
      }
    };
  }
  
  private static abstract class InputOutput {
    private InputStream in;
    private InputOutput(final InputStream in) {
      this.in = in;      
      ExecutorService service = Executors.newSingleThreadExecutor();
      service.execute(()-> {
        try {
          BufferedReader reader = 
            new BufferedReader(new InputStreamReader(in));
          while (true) {
            String line = reader.readLine();
            if (line == null) break;
            output(line);
          }
        } catch (IOException ex) {}
      });
      service.shutdown();
      
    }
    protected abstract void output(String line);
    void close() { try { in.close(); } catch (IOException ex) {} }
  }

  public void flush() {
    if (stderr != null) { stderr.close(); stderr = null; }
    if (stdout != null) { stdout.close(); stdout = null; }
  }
  
  /** 標準エラー出力内容を吐き出す */
  protected abstract void stderr(String line);
  
  /** 標準出力内容を吐き出す */
  protected abstract void stdout(String line);
  
  /////////////////////////////////////////////////////////////////////////////
  // このプロセスのstdout/errに出力
  /////////////////////////////////////////////////////////////////////////////
  
  public static class Redirection extends ProcessOutput {
    
    public Redirection(Process process) {
     super(process); 
    }
    
    @Override protected void stderr(String line) {      
      System.out.println(line);
    }
    
    @Override protected void stdout(String line) { 
      System.err.println(line);
    }
  }
  
  /////////////////////////////////////////////////////////////////////////////
  // バッファリングする
  /////////////////////////////////////////////////////////////////////////////
  
  public static class Buffer extends ProcessOutput {
    
    private StringBuilder stderr = new StringBuilder();
    private StringBuilder stdout = new StringBuilder();
    
    public Buffer(Process process) {
      super(process);
    }
    
    @Override protected synchronized void stderr(String line) {      
      System.out.println("" + line);
      stderr.append(line + "\n");
    }
    
    @Override protected synchronized void stdout(String line) { 
      stdout.append(line + "\n");
    }
    
    public String getStdErr() {
      flush();
      return stderr.toString();
    }
    public String getStdOut() {
      flush();
      return stdout.toString();
    }
  }
}
