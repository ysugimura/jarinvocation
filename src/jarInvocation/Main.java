package jarInvocation;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.*;

import javax.swing.*;

public class Main {

  public static void main(String[]args) {
    new Main().execute();
  }

  private String pid =  java.lang.management.ManagementFactory.getRuntimeMXBean().getName();

  void execute() {

    Runtime.getRuntime().addShutdownHook(new Thread() { public void run() {        
      System.out.println("stopping " + pid);
    }});
    System.out.println("starting " + pid);
    
    JFrame frame = new JFrame("jar invocation checker");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    Container contentPane = frame.getContentPane();
    
    JTextArea textArea = new JTextArea(getVersions() + getPath());
    JScrollPane scrollPane = new JScrollPane(textArea);
    textArea.setEditable(false);    
    contentPane.add(scrollPane, BorderLayout.CENTER);
    
    JPanel buttons = new JPanel();
    buttons.setLayout(new FlowLayout());
    contentPane.add(buttons, BorderLayout.SOUTH);
    
    JButton javaButton = new JButton("java");    
    javaButton.addActionListener(this::java);
    buttons.add(javaButton);
    
    JButton javawButton = new JButton("javaw");    
    javawButton.addActionListener(this::javaw);
    buttons.add(javawButton);
    
    frame.setSize(new Dimension(500, 500));
    frame.setLocationRelativeTo(null);
    Point p = frame.getLocation();
    Random r = new Random();
    frame.setLocation(p.x + r.nextInt(100), p.y + r.nextInt(100));
    
    frame.setVisible(true);
  }
  
  static final String[]VERSIONS = {
      "java.home",
      "java.version",
      "java.vender.version",
      "java.vm.version",
      "os.arch",
      "java.library.path"
  };
  
  String getVersions() {   
    return Arrays.stream(VERSIONS).map(key->key + "=" + System.getProperty(key))
        .collect(Collectors.joining("\n")) + "\n";
  }

  String getPath() {
    return "\nPATH...\n" + System.getenv("PATH").replace(File.pathSeparatorChar, '\n');
  }
  
  void java(ActionEvent e) {
    spawn(false);
  }
  void javaw(ActionEvent e) {
    spawn(true);
  }
  
  void spawn(boolean javaw) {
    
    String javaCommand = javaw? "javaw":"java";
    
    File file = ClassPathLocator.getLocation();
    String[]command;
    if (file.isDirectory()) {
      command = new String[] {
        javaCommand, "-cp", file.toString(), Main.class.getName() };
    } else {
      command = new String[] {
        javaCommand, "-jar", file.toString() };
    }
    
    try {
      ProcessBuilder builder = new ProcessBuilder(command);
      System.out.println("invoking process " + Arrays.stream(command).collect(Collectors.joining(" ")));
      Process p = builder.start();
      new ProcessOutput(p) {
        protected void stderr(String line) {
          System.out.println(line);           
        }
        protected void stdout(String line) {
          System.out.println(line); 
        }        
      };
      
      ExecutorService service = Executors.newSingleThreadExecutor();
      service.execute(()-> {
        try {
          p.waitFor();
          System.out.println("invoked process stopped");
        } catch (InterruptedException ex) {}
      });
      service.shutdown();
      
    } catch (Exception ex) {
      JOptionPane.showMessageDialog(null,  "Could not spawn");
    }
  }
}
